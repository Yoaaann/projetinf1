package lucioles;

// Étape 1 : Simulation d'une seule luciole

public class LucioleSeule {

	// Seuil au delà duquel une luciole émet un flash.
	public static final double SEUIL = 100.0;
	
	public static char symboliseLuciole(double niveauEnergie) {
		if (niveauEnergie<SEUIL) {
			return '.';
		}
		return '*';
	}
	
	public static void afficheLuciole(double niveauxEnergie,boolean verbeux) {
		if(verbeux) {
			System.out.print(symboliseLuciole(niveauxEnergie)+niveauxEnergie);
		}
		else {
			System.out.print(symboliseLuciole(niveauxEnergie));
		}
		}
	
	public static void main(String[] args) {
		// TODO À compléter
		double lucioleEnergie=RandomGen.rGen.nextDouble()*100;
		afficheLuciole(lucioleEnergie, false);
		lucioleEnergie+=45;
		
		afficheLuciole(lucioleEnergie, false);
		lucioleEnergie+=10;
		
		afficheLuciole(lucioleEnergie, false);
		lucioleEnergie-=35;
		
		afficheLuciole(lucioleEnergie, false);
		lucioleEnergie+=45;
		
		afficheLuciole(lucioleEnergie, false);
			
	}

}
